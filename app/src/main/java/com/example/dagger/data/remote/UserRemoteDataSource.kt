package com.example.dagger.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface UserRemoteDataSource {
    companion object{
        operator fun invoke(): UserRemoteDataSource {
            val baseUrl = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"

            return Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build().create(
                UserRemoteDataSource::class.java)
        }
    }
}