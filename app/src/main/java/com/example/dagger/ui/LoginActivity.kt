package com.example.dagger.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.dagger.R
import com.example.dagger.data.repository.UserRepository
import com.example.dagger.ui.common.TrainingApplication
import com.example.dagger.ui.login.DaggerLoginComponent
import com.example.dagger.ui.login.LoginModule
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    //Classe que será injetada pelo Dagger
    @Inject
    lateinit var userRepository: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Builda o dagger
        val component = DaggerLoginComponent.builder()
            .trainingApplicationComponent(TrainingApplication.trainingApplicationComponent) //É instanciado assim que o app é criado!, Garante que o COMPONENTE é singleton
            .loginModule(LoginModule()).build()

        //Injeta na classe
        component.inject(this)
    }
}
