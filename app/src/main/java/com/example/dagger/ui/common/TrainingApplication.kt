package com.example.dagger.ui.common

import android.app.Application

//Essa classe é usada para métodos "genéricos" que outras acitivities podem ter que usar, poor isso extende de Application
class TrainingApplication: Application() {
    companion object{
        lateinit var trainingApplicationComponent: TrainingApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
            trainingApplicationComponent = DaggerTrainingApplicationComponent.builder()
                .trainingApplicationModule(TrainingApplicationModule(this))
                .build()
    }
}