package com.example.dagger.ui.common

import android.content.SharedPreferences
import com.example.dagger.ui.common.qualifier.CustomPreferences
import com.example.dagger.ui.common.qualifier.NormalPreferences
import com.example.dagger.ui.common.scope.AppScope
import dagger.Component

//Como só é usado no LoginModule, ele só precisa saber como dar os dados para ele
@Component(modules = [TrainingApplicationModule::class])
@AppScope
interface TrainingApplicationComponent{
    @NormalPreferences fun getSharedPreferences(): SharedPreferences
    @CustomPreferences fun getCustomPreferences(): SharedPreferences
}