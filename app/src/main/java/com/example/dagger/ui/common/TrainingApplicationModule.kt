package com.example.dagger.ui.common

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import com.example.dagger.ui.common.qualifier.CustomPreferences
import com.example.dagger.ui.common.qualifier.NormalPreferences
import com.example.dagger.ui.common.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class TrainingApplicationModule(private val context: Context) {

    @Provides
    @AppScope
    @NormalPreferences
    fun getSharedPreferences(): SharedPreferences {
        Log.d("HelpMe", "Entrei no sharedPref")
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @AppScope
    @CustomPreferences
    fun getCustomPreferences(): SharedPreferences {
        Log.d("HelpMe", "Entrei no custom")
        return PreferenceManager.getDefaultSharedPreferences(context)
    }
}