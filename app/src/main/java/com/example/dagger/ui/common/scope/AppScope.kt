package com.example.dagger.ui.common.scope

import javax.inject.Scope

//Qualquer provider que tiver essa anotação vai ter o mesmo escopo que o componente anotado, no caso, trainingComponent
//Faz com que os métodos virem singleton
@Scope
annotation class AppScope

@Scope
annotation class ActivityScope
