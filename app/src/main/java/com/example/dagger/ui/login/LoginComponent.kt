package com.example.dagger.ui.login

import com.example.dagger.ui.LoginActivity
import com.example.dagger.ui.common.TrainingApplicationComponent
import com.example.dagger.ui.common.TrainingApplicationModule
import com.example.dagger.ui.common.scope.ActivityScope
import dagger.Component

//Diz qual modulo o componente vai usar e quais as dependencias
@Component(modules = [LoginModule::class], dependencies = [TrainingApplicationComponent::class])
@ActivityScope
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
}