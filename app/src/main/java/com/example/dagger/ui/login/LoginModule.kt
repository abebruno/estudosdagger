package com.example.dagger.ui.login

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import com.example.dagger.data.local.UserLocalDataSource
import com.example.dagger.data.remote.UserRemoteDataSource
import com.example.dagger.data.repository.UserRepository
import com.example.dagger.ui.common.qualifier.CustomPreferences
import com.example.dagger.ui.common.qualifier.NormalPreferences
import com.example.dagger.ui.common.scope.ActivityScope
import dagger.Module
import dagger.Provides

//Define o módulo
@Module
class LoginModule{

    @Provides
    @ActivityScope
    fun getUserLocalDataSource(@CustomPreferences sharedPreferences: SharedPreferences): UserLocalDataSource{
        return UserLocalDataSource(sharedPreferences)
    }

    @Provides
    @ActivityScope
    fun getUserRemoteDataSource(): UserRemoteDataSource{
        return UserRemoteDataSource()
    }

    @Provides
    @ActivityScope
    fun getUserRepository(userLocalDataSource: UserLocalDataSource, userRemoteDataSource: UserRemoteDataSource):UserRepository{
        Log.d("HelpMe", "Entrei no getRepo")
        return UserRepository(userLocalDataSource, userRemoteDataSource)
    }
}